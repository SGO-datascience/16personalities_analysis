# 16 personalities analysis
We analyze the [J. Mitchell's kaggle dataset](https://www.kaggle.com/datasnaek/mbti-type) [1] including the personality types and few social network posts.
![16 personalities](./data/16-personalities-meyers-brigg-characters.jpg "16 personalities")


## Ideas
* **The data are probably biased, the social netowork posts are downloaded from [PersonalityCafe forum](http://personalitycafe.com/forum/) and we can suppose some correlation between people who sign to this forum and their personality type.**
* Basic statistics
* Can we predict personality type facebook feed?

## References
[1] J. Mitchell, *“(MBTI) Myers-Briggs Personality Type Dataset”,* Kaggle Datasets. [Online]. Available: [https://www.kaggle.com/datasnaek/mbti-type](https://www.kaggle.com/datasnaek/mbti-type). [Accessed: 09-Dec-2017].